/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemipc.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/26 19:40:41 by garm              #+#    #+#             */
/*   Updated: 2014/06/01 18:54:51 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEMIPC_H
# define LEMIPC_H

# include <unistd.h>
# include <stdlib.h>
# include <signal.h>
# include <sys/types.h>
# include <sys/ipc.h>
# include <sys/shm.h>
# include <sys/sem.h>
# include <sys/msg.h>
# include <mlx.h>
# include "libft.h"

# define COLOR(R,G,B) ((R) * 65536 + (G) * 256 + (B))
# define KEY_ESC 65307

# define WIN_TITLE "LemIPC"
# define WIN_MAX_X 2560
# define WIN_MAX_Y 1440

# define UP 1
# define LEFT 2
# define RIGHT 3
# define DOWN 4

# define MAP_X 42
# define MAP_Y 42
# define MAPUNIT 12
# define MAXTEAM 8

# define ERROR -1
# define BUFLEN 1024

# define CAN_UP(y) ((y) > 0)
# define CAN_DOWN(y) ((y) < (MAP_Y - 1))
# define CAN_LEFT(x) ((x) > 0)
# define CAN_RIGHT(x) ((x) < (MAP_X - 1))

typedef struct		s_player
{
	int				x;
	int				y;
	int				team;
}					t_player;

typedef struct		s_team
{
	int				color;
	int				players;
}					t_team;

typedef struct		s_shared
{
	int				map[MAP_Y][MAP_X];
	t_team			teams[MAXTEAM];
}					t_shared;

typedef struct		s_env
{
	void			*mlx;
	void			*win;
	struct s_img	*screen;
	int				width;
	int				height;
	t_shared		*memory;
}					t_env;

typedef struct		s_img
{
	t_env			*e;
	void			*ptr;
	char			*data;
	int				bpp;
	int				sizel;
	int				endian;
	int				w;
	int				h;
}					t_img;

typedef t_shared	t_shm;

/*
** ft_image.c
*/
t_img				*ft_img_create(t_env *e, int w, int h);
void				ft_img_pixel_put(t_img *img, int x, int y, int color);
void				ft_img_reset(t_img *img);
void				ft_img_display(t_img *img);
void				ft_img_destroy(t_img *img);

/*
** ft_mlx.c
*/
void				ft_mlx_core(t_shared *memory);
void				ft_init_colors(t_shared *memory);

/*
** ft_semaphore.c
*/
int					ft_get_semid(char *pathname);
void				ft_sem_wait(int semid);
void				ft_sem_signal(int semid);

/*
** ft_shm.c
*/
t_shared			*ft_wait_shm(key_t key);
t_shared			*ft_get_shm(key_t key);
void				ft_destroy_shm(key_t key, int semid);

/*
** ft_player.c
*/
void				ft_destroy_queue(key_t key);
void				ft_player_new(t_shm *mem, int semid, int team, char *path);
void				ft_player_left(t_shm *mem, int semid, int team, char *path);
int					ft_count_players(t_team *teams);

/*
** ft_ia.c
*/
void				ft_play_h(t_shm *mem, char **argv, int semid, int team);
void				ft_play_v(t_shm *mem, char **argv, int semid, int team);

#endif
