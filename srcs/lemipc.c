/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemipc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/26 19:39:06 by garm              #+#    #+#             */
/*   Updated: 2014/06/01 18:55:30 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <errno.h>
#include "lemipc.h"

int		ft_main_process(char *pathname)
{
	key_t		key;
	t_shared	*memory;

	ft_putendl("Main Process\n------------");
	key = ftok(pathname, 'A');
	if (key == ERROR)
		ft_fatal("ftok error.");
	memory = ft_wait_shm(key);
	ft_mlx_core(memory);
	return (0);
}

void	ft_find_ennemy(t_shared *m, t_player *me, t_player *e)
{
	if ((CAN_UP(e->y)) && m->map[e->y - 1][e->x] == 0)
	{
		e->y--;
		e->team = 0;
		ft_find_ennemy(m, me, e);
	}
}

void	ft_play(t_shared *memory, char **argv, int semid)
{
	t_player		me;
	t_player		ennemy;

	me.team = ft_atoi(argv[2]);
	me.x = ft_atoi(argv[2]);
	me.y = ft_atoi(argv[3]);
	ft_memcpy(&ennemy, &me, sizeof(t_player));
	ennemy.team = ERROR;
	ft_find_ennemy(memory, &me, &ennemy);
	(void)semid;
}

int		ft_second_process(char **argv)
{
	char		*pathname;
	key_t		key;
	int			semid;
	t_shared	*memory;

	pathname = argv[0];
	key = ftok(pathname, 'A');
	if (key == ERROR)
		ft_fatal("ftok error.");
	memory = ft_get_shm(key);
	if ((semid = ft_get_semid(argv[0])) == ERROR)
		ft_fatal("semget error.");
	ft_player_new(memory, semid, ft_atoi(argv[1]), pathname);
	if (ft_strcmp(argv[4], "-v") == 0)
		ft_play_v(memory, argv, semid, ft_atoi(argv[1]));
	else if (ft_strcmp(argv[4], "-h") == 0)
		ft_play_h(memory, argv, semid, ft_atoi(argv[1]));
	ft_player_left(memory, semid, ft_atoi(argv[1]), pathname);
	if ((shmdt((char *)memory)) == ERROR)
		ft_fatal("shmdt error.");
	return (0);
}

int		main(int argc, char **argv)
{
	signal(SIGINT, SIG_IGN);
	if (argc == 1)
		return (ft_main_process(argv[0]));
	else if (argc == 5)
		return (ft_second_process(argv));
	else
		ft_fatal("usage : \t./lemipc\n\t\t./lemipc <team> <x> <y> <mode>");
	return (0);
}
