/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_semaphore.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/28 08:21:09 by garm              #+#    #+#             */
/*   Updated: 2014/05/29 22:01:03 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemipc.h"

static int		ft_init_sem(key_t key)
{
	int				semid;
	unsigned short	semval;

	semval = 1;
	semid = semget(key, 1, IPC_CREAT | 0666);
	if (semid == ERROR)
		ft_fatal("Semaphore already exist.");
	if ((semctl(semid, 0, SETVAL, semval)) == ERROR)
		ft_fatal("semctl SETVAL error.");
	return (semid);
}

int				ft_get_semid(char *pathname)
{
	key_t			key;
	int				semid;
	unsigned short	semval;

	semval = 1;
	key = ftok(pathname, 'B');
	semid = semget(key, 1, 0);
	if (semid == ERROR)
		semid = ft_init_sem(key);
	if (semid == ERROR)
		ft_fatal("semget error.");
	if ((semctl(semid, 0, SETVAL, semval)) == ERROR)
		ft_fatal("semctl error.");
	return (semid);
}

void			ft_sem_wait(int semid)
{
	struct sembuf	wait[1];

	wait[0].sem_num = 0;
	wait[0].sem_op = -1;
	wait[0].sem_flg = SEM_UNDO;
	if ((semop(semid, wait, 1)) == ERROR)
		exit(1);
}

void			ft_sem_signal(int semid)
{
	struct sembuf	signal[1];

	signal[0].sem_num = 0;
	signal[0].sem_op = 1;
	signal[0].sem_flg = SEM_UNDO;
	if ((semop(semid, signal, 1)) == ERROR)
		ft_fatal("semop signaling error.");
}
