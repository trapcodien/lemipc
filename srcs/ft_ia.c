/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ia.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/01 18:29:14 by garm              #+#    #+#             */
/*   Updated: 2014/06/01 18:50:46 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemipc.h"

static void		ft_h(t_shared *memory, t_player *me, int semid)
{
	int		life;
	int		dir;

	life = 250;
	dir = 1;
	while (life > 0)
	{
		ft_sem_wait(semid);
		if (memory->map[me->y][me->x + dir] == 0)
		{
			memory->map[me->y][me->x + dir] = me->team;
			memory->map[me->y][me->x] = 0;
			me->x += dir;
			if (me->x == 0 || me->x + dir == MAP_X
						|| memory->map[me->y][me->x + dir] != 0)
				dir *= -1;
		}
		ft_sem_signal(semid);
		usleep(50000);
		life--;
	}
}

void			ft_play_h(t_shared *memory, char **argv, int semid, int team)
{
	t_player	me;

	me.x = ft_atoi(argv[2]);
	me.y = ft_atoi(argv[3]);
	me.team = team;
	if (me.x >= MAP_X || me.y >= MAP_Y)
		ft_fatal("Wrong coordinates.");
	if (team > MAXTEAM)
		ft_fatal("Team number too large.");
	ft_h(memory, &me, semid);
	ft_sem_wait(semid);
	memory->map[me.y][me.x] = 0;
	memory->teams[team].players--;
}

static void		ft_v(t_shared *memory, t_player *me, int semid)
{
	int		life;
	int		dir;

	life = 250;
	dir = 1;
	while (life > 0)
	{
		ft_sem_wait(semid);
		if (memory->map[me->y + dir][me->x] == 0)
		{
			memory->map[me->y + dir][me->x] = me->team;
			memory->map[me->y][me->x] = 0;
			me->y += dir;
			if (me->y == 0 || me->y + dir == MAP_Y
						|| memory->map[me->y + dir][me->x] != 0)
				dir *= -1;
		}
		ft_sem_signal(semid);
		usleep(50000);
		life--;
	}
}

void			ft_play_v(t_shared *memory, char **argv, int semid, int team)
{
	t_player	me;

	me.x = ft_atoi(argv[3]);
	me.y = ft_atoi(argv[2]);
	me.team = team;
	if (me.x >= MAP_X || me.y >= MAP_Y)
		ft_fatal("Wrong coordinates.");
	if (team > MAXTEAM)
		ft_fatal("Team number too large.");
	ft_v(memory, &me, semid);
	ft_sem_wait(semid);
	memory->map[me.y][me.x] = 0;
	memory->teams[team].players--;
}
