/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_player.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/01 18:25:28 by garm              #+#    #+#             */
/*   Updated: 2014/06/01 18:29:08 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemipc.h"

void	ft_destroy_queue(key_t key)
{
	int		qid;

	qid = msgget(key, 0666);
	if (qid != ERROR)
		msgctl(qid, IPC_RMID, NULL);
}

void	ft_player_new(t_shared *memory, int semid, int team, char *pathname)
{
	key_t	key;
	int		qid;

	qid = ERROR;
	key = ftok(pathname, (char)team);
	if (key == ERROR)
		ft_fatal("ftok error.");
	ft_sem_wait(semid);
	memory->teams[team].players++;
	if (memory->teams[team].players == 1)
		qid = msgget(key, IPC_CREAT | IPC_EXCL | 0666);
	else
		qid = msgget(key, 0666);
	if (qid == ERROR)
		ft_fatal("msgget error.");
	ft_sem_signal(semid);
}

void	ft_player_left(t_shared *memory, int semid, int team, char *pathname)
{
	key_t	key_queue;
	key_t	key_shm;

	if ((key_shm = ftok(pathname, 'A')) == ERROR)
		ft_fatal("ftok error.");
	if ((key_queue = ftok(pathname, (char)team)) == ERROR)
		ft_fatal("ftok error.");
	if (memory->teams[team].players == 0)
		ft_destroy_queue(key_queue);
	if (ft_count_players(memory->teams) == 0)
		ft_destroy_shm(key_shm, semid);
	else
		ft_sem_signal(semid);
}

int		ft_count_players(t_team *teams)
{
	int		count;
	int		i;

	i = 0;
	count = 0;
	while (i < MAXTEAM)
	{
		count += teams[i].players;
		i++;
	}
	return (count);
}
