/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_shm.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/01 18:21:57 by garm              #+#    #+#             */
/*   Updated: 2014/06/01 18:36:22 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemipc.h"

t_shared	*ft_wait_shm(key_t key)
{
	t_shared	*memory;
	int			id_segment;

	ft_putendl("Waiting shared memory...");
	while ((id_segment = shmget(key, sizeof(t_shared), 0666)) == -1)
		usleep(500000);
	memory = (t_shared *)shmat(id_segment, NULL, SHM_R | SHM_W);
	if (memory == NULL)
		ft_fatal("shmat error.");
	ft_putendl("Shared memory segment found. Starting GUI...");
	return (memory);
}

t_shared	*ft_get_shm(key_t key)
{
	t_shared	*memory;
	int			id_segment;
	int			first;

	first = 0;
	id_segment = shmget(key, sizeof(t_shared), 0666);
	if (id_segment == ERROR)
	{
		id_segment = shmget(key, sizeof(t_shared), IPC_CREAT | IPC_EXCL | 0666);
		first = 1;
	}
	if (id_segment == ERROR)
		ft_fatal("shmget error. (create)");
	memory = (t_shared *)shmat(id_segment, NULL, SHM_R | SHM_W);
	if (memory == NULL)
		ft_fatal("shmat error.");
	if (first)
	{
		ft_bzero(memory, sizeof(t_shared));
		ft_init_colors(memory);
	}
	return (memory);
}

void		ft_destroy_shm(key_t key, int semid)
{
	int		id_segment;

	ft_putendl("DESTROY SHARED MEMORY.");
	id_segment = shmget(key, sizeof(t_shared), 0666);
	if (id_segment == ERROR)
		ft_fatal("shmget error. (destroy)");
	if ((shmctl(id_segment, IPC_RMID, NULL)) == ERROR)
		ft_fatal("shmctl (IPC_RMID) error.");
	if (semctl(semid, 0, IPC_RMID, 0) == ERROR)
		ft_fatal("semctl (IPC_RMID) error.");
}
