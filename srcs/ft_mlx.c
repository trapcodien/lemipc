/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mlx.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/28 01:59:21 by garm              #+#    #+#             */
/*   Updated: 2014/05/30 23:40:25 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemipc.h"

static int	ft_key_hook(int keycode, t_env *e)
{
	if (keycode == KEY_ESC)
	{
		ft_putendl("Goodbye.");
		if ((shmdt((char *)e->memory)) == ERROR)
			ft_fatal("shmdt error.");
		exit(0);
	}
	return (0);
	(void)e;
}

static void	ft_place_player(t_env *e, int x, int y, int color)
{
	int		i;
	int		j;

	x *= MAPUNIT;
	y *= MAPUNIT;
	i = 0;
	while (i < MAPUNIT)
	{
		j = 0;
		while (j < MAPUNIT)
		{
			if (i != 0 && i < MAPUNIT - 1 && j != 0 && j < MAPUNIT - 1)
				ft_img_pixel_put(e->screen, x + j, y + i, color);
			j++;
		}
		i++;
	}
}

static int	ft_loop_hook(t_env *e)
{
	t_shared	*memory;
	int			y;
	int			x;
	int			color;

	ft_img_reset(e->screen);
	memory = e->memory;
	y = 0;
	while (y < MAP_Y)
	{
		x = 0;
		while (x < MAP_X)
		{
			if (memory->map[y][x] != 0)
			{
				color = memory->teams[memory->map[y][x] - 1].color;
				ft_place_player(e, x, y, color);
			}
			x++;
		}
		y++;
	}
	ft_img_display(e->screen);
	return (0);
}

void		ft_init_colors(t_shared *memory)
{
	t_team		*teams;

	teams = memory->teams;
	teams[0].color = COLOR(200, 21, 21);
	teams[1].color = COLOR(237, 18, 222);
	teams[2].color = COLOR(34, 147, 39);
	teams[3].color = COLOR(86, 255, 63);
	teams[4].color = COLOR(0, 114, 255);
	teams[5].color = COLOR(0, 255, 233);
	teams[6].color = COLOR(97, 0, 255);
	teams[7].color = COLOR(213, 37, 252);
}

void		ft_mlx_core(t_shared *memory)
{
	t_env	e;

	e.memory = memory;
	ft_init_colors(memory);
	e.mlx = mlx_init();
	e.width = MAP_X * MAPUNIT;
	e.height = MAP_Y * MAPUNIT;
	if (e.width > WIN_MAX_X || e.height > WIN_MAX_Y)
		ft_fatal("Bad dimensions.");
	e.win = mlx_new_window(e.mlx, e.width, e.height, WIN_TITLE);
	e.screen = ft_img_create(&e, e.width, e.height);
	mlx_key_hook(e.win, ft_key_hook, &e);
	mlx_loop_hook(e.mlx, ft_loop_hook, &e);
	mlx_loop(e.mlx);
}
