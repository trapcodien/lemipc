# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: garm <garm@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/08/26 23:49:31 by garm              #+#    #+#              #
#    Updated: 2014/06/01 18:30:24 by garm             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc

NAME = lemipc
LIBS = -lft -lmlx -lXext -lX11
FTLIBS = libft.a

LIB_DIR = libft
SOURCES_DIR = srcs
INCLUDES_DIR = includes

LIB_DEP = libft/libft.a

ifeq ($(DEBUG), 1)
	FLAGS = -g -Wall -Wextra -pedantic
	CC = cc
else
	FLAGS = -Wall -Werror -Wextra -ansi -pedantic -std=c89
endif

ifeq ($(STRICT), 1)
	FLAGS += -fstack-protector-all -Wshadow -Wunreachable-code \
			  -Wstack-protector -pedantic-errors -O0 -W -Wundef -fno-common \
			  -Wfatal-errors -Wstrict-prototypes -Wmissing-prototypes \
			  -Wwrite-strings -Wunknown-pragmas -Wdeclaration-after-statement \
			  -Wold-style-definition -Wmissing-field-initializers \
			  -Wpointer-arith -Wnested-externs -Wstrict-overflow=5 -fno-common \
			  -Wno-missing-field-initializers -Wswitch-default -Wswitch-enum \
			  -Wbad-function-cast -Wredundant-decls -fno-omit-frame-pointer \
			  -Wfloat-equal
endif

CFLAGS =  $(FLAGS) -I $(INCLUDES_DIR) -I ./$(LIB_DIR)/includes
LDFLAGS = -L $(LIB_DIR) -L /usr/X11/lib $(LIBS)

DEPENDENCIES = \
			   $(INCLUDES_DIR)/lemipc.h

SOURCES = \
		  $(SOURCES_DIR)/lemipc.c \
		  $(SOURCES_DIR)/ft_mlx.c \
		  $(SOURCES_DIR)/ft_ia.c \
		  $(SOURCES_DIR)/ft_player.c \
		  $(SOURCES_DIR)/ft_shm.c \
		  $(SOURCES_DIR)/ft_semaphore.c \
		  $(SOURCES_DIR)/ft_image.c

OBJS = $(SOURCES:.c=.o)

all: $(NAME)

%.o: %.c $(DEPENDENCIES)
	$(CC) -c $< -o $@ $(CFLAGS)

$(NAME): $(OBJS) $(LIB_DEP)
	@echo Creating $(NAME)...
	@$(CC) -o $(NAME) $(OBJS) $(LDFLAGS)

$(LIB_DEP):
	@make $(FTLIBS) -C $(LIB_DIR)

test:
	@$(MAKE)
	@echo
	@echo ------- Program Start -------
	@echo
	@./$(NAME)

clean:
	@make clean -C $(LIB_DIR)
	@rm -f $(OBJS)
	@echo Deleting $(NAME) OBJECTS files...

fclean: clean
	@make fclean -C $(LIB_DIR)
	@rm -f $(NAME)
	@echo Deleting $(NAME)...

re: fclean all

.PHONY: clean fclean re all test

